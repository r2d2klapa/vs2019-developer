﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="EjercicioFinal.WebForms.Mantenimiento.Customer.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Lista de Customer</h1>
    <asp:Repeater runat="server" ID="rptCustomers">
        <HeaderTemplate>
            <table class="table">
                <thead>
                    <tr>
                        <th>Id
                        </th>
                        <th>FirstName
                        </th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("CustomerId") %></td>
                <td><%# Eval("FirstName") %></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:TextBox runat="server" ID="txtValor" OnTextChanged="txtValor_TextChanged" AutoPostBack="true"></asp:TextBox>
            <asp:Label runat="server" ID="lblValor"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gvCustomer" CssClass="table" GridLines="None" AllowPaging="true" PageSize="10" OnPageIndexChanging="gvCustomer_PageIndexChanging" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField HeaderText="Id" DataField="CustomerId" />
                    <asp:BoundField HeaderText="Nombre" DataField="FirstName" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Repeater runat="server" ID="rptCustomersV2">
        <HeaderTemplate>
            <div class="list-group">
        </HeaderTemplate>
        <ItemTemplate>
            <div class='<%# Convert.ToInt32(Eval("SupportRepId")) > 4 ? "bg-success" : "bg-warning" %>'>
                <h4 class="list-group-item-heading">ID:&nbsp;<%# Eval("CustomerId") %></h4>
                <p class="list-group-item-text">El primer nombre es:&nbsp;<%# Eval("FirstName") %></p>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:Repeater>


</asp:Content>
