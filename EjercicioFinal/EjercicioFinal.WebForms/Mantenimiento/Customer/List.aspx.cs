﻿using EjercicioFinal.DataAccess.EF;
using EjercicioFinal.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EjercicioFinal.WebForms.Mantenimiento.Customer
{
    public partial class List : System.Web.UI.Page
    {
        protected readonly IChinookUnitOfwork _unit;

        public List()
        {
            _unit = new ChinookUnitOfwork(new ChinookDbContext());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var lista = _unit.CustomerRepository.GetAll().ToList();
                // cargar la data de customers
                rptCustomers.DataSource = lista;
                rptCustomers.DataBind();

                // cargar la data de customers V2
                rptCustomersV2.DataSource = lista;
                rptCustomersV2.DataBind();

                // cargar la data en la grilla
                gvCustomer.DataSource = lista;
                gvCustomer.DataBind();
            }
        }

        protected void gvCustomer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCustomer.PageIndex = e.NewPageIndex;
            var lista = _unit.CustomerRepository.GetAll().ToList();
            gvCustomer.DataSource = lista;
            gvCustomer.DataBind();
        }

        protected void txtValor_TextChanged(object sender, EventArgs e)
        {
            // obtener el valor actual
            var valorActual = txtValor.Text;
            lblValor.Text = $"El valor del txt es: {valorActual}";
        }
    }
}