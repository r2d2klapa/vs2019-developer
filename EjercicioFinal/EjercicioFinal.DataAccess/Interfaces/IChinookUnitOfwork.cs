﻿using EjercicioFinal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioFinal.DataAccess.Interfaces
{
    public interface IChinookUnitOfwork
    {
        IRepository<Customer> CustomerRepository { get; set; }
        IRepository<Track> TrackRepository { get; set; }
    }
}
