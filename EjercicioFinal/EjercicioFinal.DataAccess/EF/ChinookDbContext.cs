﻿using EjercicioFinal.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioFinal.DataAccess.EF
{
    public class ChinookDbContext : DbContext
    {
        public ChinookDbContext() : base("ChinookConnection")
        {
            Database.SetInitializer<ChinookDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // cuando la llave de la tabla no es la convencional
            modelBuilder.Entity<NoAuto>().HasKey(n => n.Codigo);
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<NoAuto> NoAutos { get; set; }
        public virtual DbSet<Track> Tracks { get; set; }
    }
}
