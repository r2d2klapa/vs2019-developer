﻿using EjercicioFinal.DataAccess.Interfaces;
using EjercicioFinal.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioFinal.DataAccess.EF
{
    public class ChinookUnitOfwork : IChinookUnitOfwork
    {
        public IRepository<Customer> CustomerRepository { get; set; }
        public IRepository<Track> TrackRepository { get; set; }

        public ChinookUnitOfwork(DbContext context)
        {
            CustomerRepository = new Repository<Customer>(context);
            TrackRepository = new Repository<Track>(context);
        }
    }
}
