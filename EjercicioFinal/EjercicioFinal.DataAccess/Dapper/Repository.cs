﻿using EjercicioFinal.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using EjercicioFinal.Model;
using Dapper;

namespace EjercicioFinal.DataAccess.Dapper
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly string _connectionString;

        public Repository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<T> GetAll()
        {
            var sqlSelect = $"select * from [{typeof(T).Name}]";
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<T>(sqlSelect);
            }
        }
    }
}
