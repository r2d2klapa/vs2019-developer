﻿using System;
using System.Configuration;
using System.Linq;
using EjercicioFinal.DataAccess.EF;
using EjercicioFinal.DataAccess.Interfaces;
using EjercicioFinal.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EjercicioFinal.Test
{
    [TestClass]
    public class RepositoryDirectTests
    {
        private IRepository<Customer> customerRepository;

        [TestMethod]
        public void Get_All_Customers_With_EF()
        {
            // instanciar el repo con EF
            customerRepository = new EjercicioFinal.DataAccess.EF.Repository<Customer>(new ChinookDbContext());
            Assert.AreEqual(true, customerRepository.GetAll().Count() > 0);
        }

        [TestMethod]
        public void Get_All_Customers_With_Dapper()
        {
            var cs = ConfigurationManager.ConnectionStrings["ChinookConnection"].ConnectionString;
            // instanciar el repo con EF
            customerRepository = new EjercicioFinal.DataAccess.Dapper.Repository<Customer>(cs);
            Assert.AreEqual(true, customerRepository.GetAll().Count() > 0);
        }

        [TestMethod]
        public void First_Customer_Id_Must_Be_1_With_Dapper()
        {
            var cs = ConfigurationManager.ConnectionStrings["ChinookConnection"].ConnectionString;
            // instanciar el repo con EF
            customerRepository = new EjercicioFinal.DataAccess.Dapper.Repository<Customer>(cs);
            var firstCustomer = customerRepository.GetAll().First();
            Assert.AreEqual(1, firstCustomer.CustomerId);
        }
    }
}
