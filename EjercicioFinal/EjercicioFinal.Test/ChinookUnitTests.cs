﻿using System;
using System.Linq;
using EjercicioFinal.DataAccess.EF;
using EjercicioFinal.DataAccess.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EjercicioFinal.Test
{
    [TestClass]
    public class ChinookUnitTests
    {
        protected readonly IChinookUnitOfwork _unit;
        public ChinookUnitTests()
        {
            _unit = new ChinookUnitOfwork(new ChinookDbContext());
        }
        [TestMethod]
        public void Get_All_Customers()
        {
            var list = _unit.CustomerRepository.GetAll();
            Assert.AreEqual(true, list.Count() > 0);
        }

        [TestMethod]
        public void First_Customer_Id_Must_Be_1()
        {
            var firstCustomer = _unit.CustomerRepository.GetAll().First();
            Assert.AreEqual(1, firstCustomer.CustomerId);
        }

        [TestMethod]
        public void First_Customer_Name_Should_Not_Be_Null()
        {
            var firstTrack = _unit.TrackRepository.GetAll().First();
            Assert.AreEqual("For Those About To Rock (We Salute You)", firstTrack.Name);
        }
    }
}
