﻿using System;
using System.Linq;
using EjercicioFinal.DataAccess.EF;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EjercicioFinal.Test
{
    [TestClass]
    public class ContextDirectTests
    {
        [TestMethod]
        public void Get_All_Customers()
        {
            using (var context = new ChinookDbContext())
            {
                var list = context.Customers.ToList();
                Assert.AreEqual(true, list.Count > 0);
            }
        }

        [TestMethod]
        public void First_Customer_Id_Must_Be_1()
        {
            using (var context = new ChinookDbContext())
            {
                var firstCustomer = context.Customers.First();
                Assert.AreEqual(1, firstCustomer.CustomerId);
            }
        }

        [TestMethod]
        public void Get_Customers_Whose_Name_Starts_With_Letter_A()
        {
            using (var context = new ChinookDbContext())
            {
                var list = context.Customers.Where(c => c.FirstName.StartsWith("A"));
                Assert.AreEqual(3, list.Count());
            }
        }

        [TestMethod]
        public void Get_All_No_Auto()
        {
            using (var context = new ChinookDbContext())
            {
                var list = context.NoAutos;
                Assert.AreEqual(true, list.Count() == 0);
            }
        }
    }
}
