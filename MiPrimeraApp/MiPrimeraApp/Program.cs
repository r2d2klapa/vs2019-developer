﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiPrimeraApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creando Chihuaha...");
            var chihuahua = new Chihuahua(1);
            Console.WriteLine($"{chihuahua.ToString()}: {chihuahua.Ladrar()}");
            chihuahua.RecibirEntrenamiento();
            chihuahua.RecibirEntrenamiento();
            Console.WriteLine($"{chihuahua}: {chihuahua.Ladrar()}");
        }
    }
}
