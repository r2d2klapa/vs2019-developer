﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiPrimeraApp
{
    public abstract class Perro
    {
        private int nivelDeEntrenamiento;
        protected int NivelDeEntrenamiento
        {
            get
            {
                return nivelDeEntrenamiento;
            }
            set
            {
                if (value < 1) nivelDeEntrenamiento = 1;
                else
                {
                    if (value > 5) nivelDeEntrenamiento = 5;
                    else nivelDeEntrenamiento = value;
                }
            }
        }

        protected int NivelDeAgresividad { get; set; }

        // constructor
        protected Perro()
        {

        }
        protected Perro(int nivelDeEntrenamiento)
        {
            NivelDeEntrenamiento = nivelDeEntrenamiento;
            NivelDeAgresividad = 3;
        }

        public string Ladrar()
        {
            if ((NivelDeAgresividad - NivelDeEntrenamiento) > 3)
                return "guau!!";
            return "...";
        }

        public abstract void RecibirEntrenamiento();
    }

    public class Chihuahua : Perro
    {
        public Chihuahua(int nivelDeEntrenamiento) : base(nivelDeEntrenamiento)
        {
            NivelDeAgresividad = 5;
        }

        public override void RecibirEntrenamiento()
        {
            NivelDeEntrenamiento = NivelDeEntrenamiento + 1;
        }

        public override string ToString()
        {
            return "Sonido para chihuahua";
        }
    }
}
