﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListArtistRpt.aspx.cs" Inherits="WebForms.Mantenimiento.Artist.ListArtistRpt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row" style="margin-top: 2rem;">
        <div class="col-xs-12">
            <asp:Button runat="server" ID="btnNuevo" Text="Nuevo Artista" CssClass="btn btn-primary btn-xs" OnClick="btnNuevo_Click" />
        </div>
    </div>
    <div class="row" style="margin-top: 2rem;">
        <div class="col-xs-12">
            <asp:Repeater runat="server" ID="rptArtists" OnItemCommand="rptArtists_ItemCommand">
                <HeaderTemplate>
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Nombre
                                </th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <%--la plantilla para repetir cada fila de la tabla--%>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%# Eval("Name") %>
                        </td>
                        <td>
                            <a href="EditArtist?Id=<%# Eval("ArtistId") %>" target="_blank">Editar</a>
                        </td>
                        <td>
                            <asp:LinkButton runat="server" OnClientClick="return confirm('¿Estás seguro de eliminar este registro?');" Text="Eliminar" CommandName="eliminar" CommandArgument='<%# Eval("ArtistId") %>'></asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
            </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $('table').DataTable();
        })

        function afterDelete() {
            alert("Se eliminó el artista");            
        }
    </script>
</asp:Content>
