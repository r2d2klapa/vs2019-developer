﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using Models;

namespace WebForms.Mantenimiento.Artist
{
    public partial class NewArtist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtnGuardar_Click(object sender, EventArgs e)
        {
            // validar el nombre
            if (txtNombre.Text.Trim().Length <= 0)
            {
                MostrarAlerta(false);
                return;
            }
            // guardar el artista

            // instanciar objeto a guardar
            var nuevoArtista = new Models.Artist
            {
                Name = txtNombre.Text
            };

            // instanciar el unit of work
            using (var unit = new UnitOfWork(new ChinookContext()))
            {
                try
                {
                    unit.Artists.Add(nuevoArtista);
                    var resultado = unit.Complete();
                    MostrarAlerta(resultado > 0);
                }
                catch (Exception)
                {
                    MostrarAlerta(false);
                }
            }
        }

        private void MostrarAlerta(bool success)
        {
            var titulo = success ? "Guardado exitoso" : "Ocurrió un error al guardar";
            var mensaje = success ? "La información del nuevo artista se guardó satisfactoriamente" : "No se pudo guardar la información del artista";
            var clase = $@"alert alert-{(success ? "success" : "danger")}";

            var alertHtml = $"<b>{titulo}</b>: {mensaje}";

            // asignar la clase de la alerta(css)
            divAlerta.Attributes.Add("class", clase);

            // asignar el inner html de la alerta
            divAlerta.InnerHtml = alertHtml;

            // mostrar la alerta
            divAlerta.Visible = true;
        }
    }
}