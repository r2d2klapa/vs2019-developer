﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewArtist.aspx.cs" Inherits="WebForms.Mantenimiento.Artist.NewArtist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div runat="server" id="divAlerta" role="alert" visible="false">
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 2rem">
        <div class="col-xs-12">
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="txtNombre" Text="Nombre"></asp:Label>
                <asp:TextBox runat="server" ID="txtNombre" CssClass="form-control" placeholder="Nombre"></asp:TextBox>
                <%--<asp:RequiredFieldValidator runat="server" ControlToValidate="txtNombre"></asp:RequiredFieldValidator>--%>
            </div>

            <asp:LinkButton runat="server" ID="lbtnGuardar" CssClass="btn btn-default" OnClick="lbtnGuardar_Click">
                <i class="fas fa-save"></i> Guardar
            </asp:LinkButton>
        </div>
    </div>

</asp:Content>
