﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using DataAccess;
using WebForms.App_Code;

namespace WebForms.Mantenimiento.Artist
{
    public partial class ListArtistRpt : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                VerificarUsuario();
                // también quiero asegurarme que el usuario tiene el rol "Admin"
                UserIsInRole("Admin");
            }
            MostrarTabla();
        }

        void MostrarTabla()
        {
            // inicializar una lista vacía
            var listaArtistas = new List<Models.Artist>();

            using (var unit = new UnitOfWork(new ChinookContext()))
            {
                listaArtistas = unit.Artists.GetAll().ToList();
            }

            // enlazamos la data con el repeater
            rptArtists.DataSource = listaArtistas;
            rptArtists.DataBind();
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            // para redireccionar
            Response.Redirect("NewArtist");
        }

        protected void rptArtists_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                // ejecutamos el código para eliminar un artista
                // obtener el id del CommandArgument
                if (Int32.TryParse(e.CommandArgument.ToString(), out int idEliminar))
                {
                    // eliminar de la BD
                    try
                    {
                        using (var unit = new UnitOfWork(new ChinookContext()))
                        {
                            var artistaEliminar = unit.Artists.GetById(idEliminar);
                            unit.Artists.Remove(artistaEliminar);
                            unit.Complete();
                        }
                        // si no hubo errores, mostrar una alerta
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "alerta", "afterDelete();", true);
                        // recargar la lista
                        MostrarTabla();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }
    }
}