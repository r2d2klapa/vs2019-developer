﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms.Mantenimiento.Artist
{
    public partial class EditArtist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // al cargar la página vamos a cargar la info del artista
            if (!IsPostBack)
            {
                // leer el parámetro de la URL (query string)
                int id = 0;
                if (Int32.TryParse(Request.QueryString["id"].ToString(), out id))
                {
                    // significa que es un id válido
                    CargarDataInicial(id);
                }
            }
        }

        void CargarDataInicial(int id)
        {
            var artista = new Models.Artist();

            // obtener el artista de la BD
            using (var unit = new UnitOfWork(new ChinookContext()))
            {
                artista = unit.Artists.GetById(id);
            }

            // cargar los controles de la UI
            txtNombre.Text = artista.Name;
        }

        protected void lbtnGuardar_Click(object sender, EventArgs e)
        {
            // para guardar la info
            // leer el parámetro de la URL (query string)
            int id = 0;
            if (Int32.TryParse(Request.QueryString["id"].ToString(), out id))
            {
                // significa que es un id válido, entonces guardar
                var artistaEditado = new Models.Artist()
                {
                    Name = txtNombre.Text,
                    ArtistId = id
                };

                using (var unit = new UnitOfWork(new ChinookContext()))
                {
                    unit.Artists.Update(artistaEditado);
                    unit.Complete();
                }

                Response.Redirect("ListArtistRpt");
            }
        }
    }
}