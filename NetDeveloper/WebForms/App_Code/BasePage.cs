﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebForms.App_Code
{
    public class BasePage : System.Web.UI.Page
    {
        protected void VerificarUsuario()
        {            
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // si el usuario no está autenticado
                // redireccionar al login
                RedireccionarLogin();
            }
        }

        protected void UserIsInRole(string role)
        {
            // validar si el usuario tiene el rol requerido
            if (!HttpContext.Current.User.IsInRole(role))
            {
                // si no tiene el rol, lo redireccionamos a la pantalla del login
                RedireccionarLogin();
            }
        }

        private void RedireccionarLogin()
        {
            Response.Redirect("~/Account/Login");
        }
    }
}