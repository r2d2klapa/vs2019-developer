﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarControlesAutenticacion();
            }
        }

        private void CargarControlesAutenticacion()
        {
            if (Request.IsAuthenticated)
            {
                // significa que el usuario esta autenticado, mostrar los datos correspondientes
                ulCerrarSesion.Visible = true;
                lblNombreUsuario.Text = Request.GetOwinContext().Authentication.User.Identity.Name;
            }
        }

        protected void lnkCerrarSesion_Click(object sender, EventArgs e)
        {
            // obtener el contexto de autenticación para cerrar sesión
            var context = Request.GetOwinContext().Authentication;

            // cerrar sesión
            context.SignOut();

            // redireccionar al login
            Response.Redirect("~/Account/Login");
        }
    }
}