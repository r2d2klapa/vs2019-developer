﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcTrackList.ascx.cs" Inherits="WebForms.UserControls.UcTrackList" %>


<p>
    Cantidad de canciones
    <asp:Label runat="server" ID="lblCantidadCanciones"></asp:Label>
</p>
<asp:HiddenField runat="server" ID="hdnPlaylistId" />
<asp:GridView runat="server" ID="gvTracks" CssClass="table" GridLines="None" BorderWidth="0" AutoGenerateColumns="false" AllowPaging="false" PageSize="10" OnPageIndexChanging="gvTracks_PageIndexChanging" OnRowCommand="gvTracks_RowCommand">
    <Columns>
        <asp:BoundField HeaderText="Nombre" DataField="Name" />
        <asp:BoundField HeaderText="Compositor" DataField="Composer" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton runat="server" CommandArgument='<%# Eval("TrackId") %>' CommandName="eliminar">
                    <i class="fas fa-trash"></i>
                </asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

