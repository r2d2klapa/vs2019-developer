﻿using DataAccess;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms.UserControls
{
    public partial class UcTrackList : System.Web.UI.UserControl
    {
        private readonly IUnitOfWork _unit;
        public UcTrackList()
        {
            _unit = new UnitOfWork(new ChinookContext());
        }
        // TODO: guardar la lista de canciones en el view state

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void ActualizarUserControl(IEnumerable<Track> tracks, int playlistId)
        {
            lblCantidadCanciones.Text = tracks.Count().ToString();
            gvTracks.DataSource = tracks.ToList();
            gvTracks.DataBind();

            hdnPlaylistId.Value = playlistId.ToString();

            // acutalizar udpate panel
            //pnlModalData.Update();
        }

        protected void gvTracks_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTracks.PageIndex = e.NewPageIndex;
            gvTracks.DataSource = null;
            gvTracks.DataBind();
        }

        protected void gvTracks_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "eliminar")
            {
                // obtener el id que se quiere eliminar
                var trackId = Convert.ToInt32(e.CommandArgument);

                var track = _unit.Tracks.GetById(trackId);
                // eliminar de la BD
                var resultado = _unit.Playlists.DeleteTrack(track, Convert.ToInt32(hdnPlaylistId.Value));

                _unit.Complete();

                var parent = this.Parent.Page as WebForms.Pages.Playlists;
                parent.CargarCanciones(Convert.ToInt32(hdnPlaylistId.Value));
            }
        }
    }
}