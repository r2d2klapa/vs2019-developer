﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.AspNet.Identity;

[assembly: OwinStartup(typeof(WebForms.Startup))]

namespace WebForms
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        void ConfigureAuth(IAppBuilder app)
        {
            // este inicio de sesión valida con la BD
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/Account/Login"),
                CookieName = "CibertecAuthCookie"
            });            

            // para decirle a la cookie que también se utilizará otro tipo de autenticación
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // habilitar el inicio de sesión con google oauth2
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
            {
                // generar estas llaves para su propio proyecto
                ClientId = "348145621399-q51maq2v7t83aetog2q8d48576k0qtla.apps.googleusercontent.com",
                ClientSecret = "rVh5zM4r07XcAzDRsGaNdUue",
            });            
        }
    }
}
