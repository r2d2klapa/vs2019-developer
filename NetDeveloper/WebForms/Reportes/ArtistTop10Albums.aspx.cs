﻿using DataAccess;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace WebForms.Reportes
{
    public partial class ArtistTop10Albums : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // cargar la data para mostrar en el gráfico
            var data = new List<ArtistTopAlbums>();

            using (var unit = new UnitOfWork(new ChinookContext()))
            {
                data = unit.Artists.GetTop10OfAlbums().ToList();
            }

            // con la data, podemos asignar las variables de JS

            // 1. Crear el arreglo de "categorías" para mostrarlas en el eje X
            var listaCategorias = new List<string>();
            // 2. Crear el arreglo de "data" para mostrar la data de cada categoría
            var listaData = new List<decimal>();

            // 3. Recorer la lista obtenida de BD para agregar la data a los arreglos
            foreach (var item in data)
            {
                listaCategorias.Add(item.ArtistName);
                listaData.Add(item.AlbumCount);
            }

            // 4. Colocamos los valores en el literal para pintarlos
            litData.Text =
            $@"<script>
                    var chartTile = 'Mostrando Top {data.Count}'
                    var artistCategoriesArray = {JsonConvert.SerializeObject(listaCategorias)};
                    var albumCountArray = {JsonConvert.SerializeObject(listaData)};
               </script>";
        }
    }
}