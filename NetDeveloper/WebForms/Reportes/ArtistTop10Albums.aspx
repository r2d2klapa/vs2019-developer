﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ArtistTop10Albums.aspx.cs" Inherits="WebForms.Reportes.ArtistTop10Albums" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div id="chart-container" style="width: 100%; height: 500px;"></div>
    <asp:Literal runat="server" ID="litData"></asp:Literal>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            var data = {
                title: {
                    text: chartTile
                },
                xAxis: {
                    categories: artistCategoriesArray
                },

                series: [{
                    data: albumCountArray
                }]
            }
            var chart = Highcharts.chart("chart-container", data);
        })
    </script>
</asp:Content>
