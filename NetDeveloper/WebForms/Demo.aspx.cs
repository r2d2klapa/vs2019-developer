﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using Models;
using Newtonsoft.Json;

namespace WebForms
{
    public partial class Demo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            h1Titulo.InnerHtml = "Hola mundo desde el code behind";
            h1Titulo.Attributes.Add("class", "text-danger");
            litResultados.Text = Request.Headers.ToString();
            //Response.Write("holaaaa");
            //Response.End();
        }

        protected void btnObtener_Click(object sender, EventArgs e)
        {
            // crear el cliente del servicio WCF
            var cliente = new WebForms.InvoiceServiceReference.InvoiceServiceClient();

            // obtener la data
            var data = cliente.GetInvoice("leonekohler@surfeu.de", 1);

            // pintar la data
            var resultados = string.Empty;

            foreach (var item in data)
            {
                resultados += $"{item.Email}, ";
            }

            litResultados.Text = resultados;
        }

        protected async void ObtenerDataRest_Click(object sender, EventArgs e)
        {
            // crear el cliente HTTP
            var cliente = new HttpClient()
            {
                // configurar la ruta base para consultar los servicios
                BaseAddress = new Uri("http://localhost:4864/CustomerServiceRest.svc/")
            };

            // obtener la data del servicio
            var response = await cliente.GetAsync("Invoice/leonekohler@surfeu.de/1");

            // leer el contenido de la respuesta y guardarla como string
            var data = await response.Content.ReadAsStringAsync();

            // convertir JSON a un objeto de c#
            var parsedData = JsonConvert.DeserializeObject<List<CustomerInvoice>>(data);

            // limpiar el texto del literal
            litResultados.Text = string.Empty;

            foreach (var item in parsedData)
            {
                litResultados.Text += $"REST: {item.Email}, ";
            }
        }
    }
}