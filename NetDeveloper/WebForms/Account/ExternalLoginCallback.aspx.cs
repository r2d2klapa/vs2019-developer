﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace WebForms.Account
{
    public partial class ExternalLoginCallback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var loginInfo = Context.GetOwinContext().Authentication.GetExternalLoginInfo();
                if (loginInfo != null)
                {
                    var claims = loginInfo.ExternalIdentity.Claims.ToList();
                    
                    // 1. Obtener los claims de Google para iniciar sesión
                    var emailClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
                    var nameClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
                    var nameIdentifierClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

                    // 2. Crear la identidad dentro de la aplicación
                    var identidad = new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Email, emailClaim.Value),
                        new Claim(ClaimTypes.Name, nameClaim.Value),
                        // un atributo único por cada usuario
                        new Claim(ClaimTypes.NameIdentifier, nameIdentifierClaim.Value),                    
                        //new Claim("matricula", usuario.Email)
                    }, "ApplicationCookie");

                    // 2.1 Forzar que todos los inicios de sesión tengan el rol admin
                    // deberían hacer una logica para manejarlo desde su BD
                    identidad.AddClaim(new Claim(ClaimTypes.Role, "Admin"));

                    // 3. Obtener el contexto de autenticación
                    var context = Request.GetOwinContext().Authentication;

                    // 4. Iniciar sesión para la identidad creada previamente
                    context.SignIn(identidad);

                    // 5. Redireccionar al recurso que se quería visualizar
                    Response.Redirect("~/Mantenimiento/Artist/ListArtistRpt");
                }
            }
        }
    }
}