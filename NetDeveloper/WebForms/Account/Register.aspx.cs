﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using Models;

namespace WebForms.Account
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            // 1. Obtener los datos ingresados
            var correo = txtCorreo.Text;
            var contrasenia = txtContrasenia.Text;
            var repetirContrasenia = txtRepContrasenia.Text;

            // 2. Verificar que el usuario no exista en la BD
            Models.User usuarioExistente = null;
            using (var unit = new UnitOfWork(new ChinookContext()))
            {
                usuarioExistente = unit.Users.GetByEmail(correo);
            }

            if (usuarioExistente != null)
            {
                var validator = new CustomValidator
                {
                    ErrorMessage = "El usuario ya existe",
                    IsValid = false
                };
                Page.Validators.Add(validator);
                return;
            }

            // 3. Registrar el usuario en BD
            var nuevoUsuario = new Models.User { Email = txtCorreo.Text.ToLower(), Password = txtContrasenia.Text };
            // agregar un rol por defecto
            nuevoUsuario.Roles = "Admin";
            using (var unit = new UnitOfWork(new ChinookContext()))
            {
                unit.Users.Add(nuevoUsuario);
                unit.Complete();
            }

            // 4. Si el usuario se creó sin problemas, inciar sesión

            var identidad = new ClaimsIdentity(new[]
            {
                    new Claim(ClaimTypes.Email, nuevoUsuario.Email),
                    new Claim(ClaimTypes.Name, nuevoUsuario.Email),
                    // un atributo único por cada usuario
                    new Claim(ClaimTypes.NameIdentifier, nuevoUsuario.Id.ToString()),                    
                    //new Claim("matricula", usuario.Email)
                }, "ApplicationCookie");

            identidad.AddClaim(new Claim(ClaimTypes.Role, nuevoUsuario.Roles));

            // 3. Obtener el contexto de autenticación
            var context = Request.GetOwinContext().Authentication;

            // 4. Iniciar sesión para la identidad creada previamente
            context.SignIn(identidad);

            // 5. Redireccionar al recurso que se quería visualizar
            Response.Redirect("~/Mantenimiento/Artist/ListArtistRpt");
        }
    }
}