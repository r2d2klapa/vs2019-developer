﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebForms.Account.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <h1>Registrar un Nuevo Usuario</h1>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-xs-12">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Correo</label>
                    <div class="col-sm-10">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtCorreo" placeholder="Correo" TextMode="Email"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Contraseña</label>
                    <div class="col-sm-10">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtContrasenia" placeholder="Contraseña" TextMode="Password"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Repetir Contraseña</label>
                    <div class="col-sm-10">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtRepContrasenia" placeholder="Repetir Contraseña" TextMode="Password"></asp:TextBox>
                        <asp:CompareValidator runat="server" ControlToCompare="txtContrasenia" ControlToValidate="txtRepContrasenia" ErrorMessage="Las contraseña no son iguales"></asp:CompareValidator>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:Button runat="server" CssClass="btn btn-primary" ID="btnRegistrar" OnClick="btnRegistrar_Click" Text="Registrar" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:ValidationSummary runat="server" />
        </div>
    </div>
</asp:Content>
