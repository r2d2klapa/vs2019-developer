﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Claims;
using DataAccess;
using Microsoft.Owin.Security;

namespace WebForms.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnIniciar_Click(object sender, EventArgs e)
        {
            // 1. Obtener el usuario de la BD
            var unit = new UnitOfWork(new ChinookContext());
            var usuario = unit.Users.ValidateUser(txtEmail.Text, txtPassword.Text);

            if (usuario != null)
            {
                // 2. Crear la identidad del usuario
                var identidad = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Email, usuario.Email),
                    new Claim(ClaimTypes.Name, usuario.Email),
                    // un atributo único por cada usuario
                    new Claim(ClaimTypes.NameIdentifier, usuario.Id.ToString()),                    
                    //new Claim("matricula", usuario.Email)
                }, "ApplicationCookie");

                // 2.1 Agregar los roles del usuario
                var arregloRoles = usuario.Roles.Split(',');

                foreach (var rol in arregloRoles)
                {
                    identidad.AddClaim(new Claim(ClaimTypes.Role, rol));
                }

                // 3. Obtener el contexto de autenticación
                var context = Request.GetOwinContext().Authentication;

                // 4. Iniciar sesión para la identidad creada previamente
                context.SignIn(identidad);

                // 5. Redireccionar al recurso que se quería visualizar
                Response.Redirect("~/Mantenimiento/Artist/ListArtistRpt");
            }
        }

        protected void btnIniciarGoogle_Click(object sender, EventArgs e)
        {
            Context.GetOwinContext().Authentication.Challenge(new AuthenticationProperties
            {
                RedirectUri = ResolveUrl("~/Account/ExternalLoginCallback")
            }, "Google");
        }
    }
}