﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Demo.aspx.cs" Inherits="WebForms.Demo" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h1 runat="server" id="h1Titulo">Pruebas con WCF</h1>

    <asp:Button Text="Obtener Data de InvoiceService" runat="server" ID="btnObtener" OnClick="btnObtener_Click" />
    <asp:Button Text="Obtener data con REST" runat="server" ID="ObtenerDataRest" OnClick="ObtenerDataRest_Click" />

    <asp:Literal runat="server" ID="litResultados"></asp:Literal>

    <asp:ImageButton runat="server" ID="imgDemo" ImageUrl="#" />

</asp:Content>
