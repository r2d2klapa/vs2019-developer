﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PruebaAjax.aspx.cs" Inherits="WebForms.PruebaAjax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:TextBox runat="server" ID="txtValor" OnTextChanged="txtValor_TextChanged" AutoPostBack="true"></asp:TextBox>
            <asp:Label runat="server" ID="lblValor"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
