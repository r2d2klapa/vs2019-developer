﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms.Pages
{
    public partial class Playlists : System.Web.UI.Page
    {
        private readonly IUnitOfWork _unit;
        public Playlists()
        {
            _unit = new UnitOfWork(new ChinookContext());
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // cargar la lista de playlist
                var playlists = _unit.Playlists.GetAll();

                // asgignar la lista al repeater
                rptPlaylists.DataSource = playlists;
                rptPlaylists.DataBind();

                // para el drop down de tracks
                var tracks = _unit.Tracks.GetAll();
                ddlTracks.DataSource = tracks;
                ddlTracks.DataBind();
            }
        }

        protected void rptPlaylists_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "canciones")
            {
                // significa que se ha presionado el botón Ver Canciones
                // entonces, obtener el id del playlist del que se desea visualizar sus canciones
                var playlistId = Convert.ToInt32(e.CommandArgument);
                CargarCanciones(playlistId);
            }
        }

        public void CargarCanciones(int playlistId)
        {
            // obtener las canciones
            var tracks = _unit.Playlists.GetPlaylistTracks(playlistId);

            // enviarlas al user control
            UcTrackList.ActualizarUserControl(tracks, playlistId);

            // mostrar el modal
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "openModal", "openModal();", true);

            // setear el hidden
            hdnPlaylistId.Value = playlistId.ToString();

            // actualizar el update panel modal
            pnlModal.Update();
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            var playlistId = Convert.ToInt32(hdnPlaylistId.Value);
            var track = _unit.Tracks.GetById(Convert.ToInt32(ddlTracks.SelectedValue));
            var resultado = _unit.Playlists.AddNewTrack(track, playlistId);

            // grabar en BD
            _unit.Complete();

            // actualizar el listado                    
            CargarCanciones(playlistId);
        }
    }
}