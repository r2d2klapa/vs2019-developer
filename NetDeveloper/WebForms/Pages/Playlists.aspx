﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Playlists.aspx.cs" Inherits="WebForms.Pages.Playlists" %>

<%@ Register Src="~/UserControls/UcTrackList.ascx" TagPrefix="cuc" TagName="UcTrackList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <h1>Playlists</h1>
            <small>Página para agregar o quitar canciones de una playlist</small>
        </div>
    </div>

    <div class="row" style="margin-top: 20px">
        <div class="col-xs-12">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:Repeater runat="server" ID="rptPlaylists" OnItemCommand="rptPlaylists_ItemCommand">
                        <HeaderTemplate>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nombre

                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%# Eval("Name") %>
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" Text="Ver canciones" CommandName="canciones" CommandArgument='<%# Eval("PlaylistId") %>'></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                    </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalCanciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Canciones</h4>
                </div>
                <div class="modal-body">
                    <p>Esta son las canciones de la playlist seleccionada</p>
                    <asp:DropDownList runat="server" ID="ddlTracks" DataValueField="TrackId" DataTextField="Name"></asp:DropDownList>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Button runat="server" ID="btnAgregar" OnClick="btnAgregar_Click" CssClass="btn btn-sm btn-default" Text="Agregar Canción" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <asp:UpdatePanel runat="server" ID="pnlModal" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:HiddenField runat="server" ID="hdnPlaylistId" />
                            <cuc:UcTrackList runat="server" ID="UcTrackList" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            // convertir el drop down de tracks a select2

        });
        function openModal() {
            $('#<%= ddlTracks.ClientID%>').select2();
            $('#modalCanciones').modal('show');
            // convertir el drop down de tracks a select2

        }
    </script>
</asp:Content>
