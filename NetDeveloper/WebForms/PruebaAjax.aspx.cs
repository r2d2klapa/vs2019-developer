﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms
{
    public partial class PruebaAjax : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void txtValor_TextChanged(object sender, EventArgs e)
        {
            // obtener el valor actual
            var valorActual = txtValor.Text;
            lblValor.Text = $"El valor del txt es: {valorActual}";
        }
    }
}