﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Models;
using DataAccess;

namespace WebForms.Services
{
    /// <summary>
    /// Descripción breve de ArtistService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    [ScriptService]
    public class ArtistService : System.Web.Services.WebService
    {

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public object HelloWorld()
        {
            return new { email = "email", mensaje = "hola a todos" };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public object GetAll()
        {
            using (var unit = new UnitOfWork(new ChinookContext()))
            {
                return unit.Artists.GetWithAlbumCount().ToList();
            }
        }
    }
}
