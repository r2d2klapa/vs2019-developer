﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Serializable]
    public class ArtistTopAlbums
    {
        public int Id { get; set; }
        public string ArtistName { get; set; }
        public int AlbumCount { get; set; }
    }
}
