﻿using System;
using System.Linq;
using DataAccess.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;

namespace DataAccess.Test
{
    [TestClass]
    public class PlaylistRepositoryTest
    {
        [TestMethod]
        public void Get_Tracks_Of_Playlist_With_Id_1()
        {
            // instanciar el repositorio
            var playlistRepo = new PlaylistRepository(new ChinookContext());

            // obtener la lista de tracks para el playlist con ID 1
            var tracks = playlistRepo.GetPlaylistTracks(1);

            // hacer la comprobación
            Assert.AreEqual(true, tracks.Count() > 0);
        }

        [TestMethod]
        public void Get_Name_Of_First_Track_Of_Playlist_With_Id_1()
        {
            // instanciar el repositorio
            var playlistRepo = new PlaylistRepository(new ChinookContext());

            // obtener la lista de tracks para el playlist con ID 1
            var tracks = playlistRepo.GetPlaylistTracks(1);

            // obtener la primera canción
            var firstTrack = tracks.FirstOrDefault();

            // hacer la comprobación
            Assert.AreEqual(@"For Those About To Rock (We Salute You)", firstTrack.Name);
        }

        [TestMethod]
        public void Insert_New_Track_To_Playlist_With_Id_2()
        {
            // instanciar los repositorios que vamos a utilizar
            var chinookContext = new ChinookContext();
            var trackRepo = new Repository<Track>(chinookContext);
            var playlistRepo = new PlaylistRepository(chinookContext);

            // obtener la canción que queremos insertar (canción con Id 1)
            var newTrack = trackRepo.GetById(1);

            // insertar esa canción al playlist con Id 2
            var insertResult = playlistRepo.AddNewTrack(newTrack, 2);

            // grabar los cambios
            var result = chinookContext.SaveChanges();

            // hacer la comprobación
            Assert.AreEqual(true, result > 0);
        }

        [TestMethod]
        public void Delete_Track_With_Id_1_From_Playlist_With_Id_2()
        {
            // instanciar los repositorios que vamos a utilizar
            var chinookContext = new ChinookContext();
            var trackRepo = new Repository<Track>(chinookContext);
            var playlistRepo = new PlaylistRepository(chinookContext);

            // obtener la canción que queremos eliminar (canción con Id 1)
            var deleteTrack = trackRepo.GetById(1);

            // eliminar esa canción al playlist con Id 2
            var insertResult = playlistRepo.DeleteTrack(deleteTrack, 2);

            // grabar los cambios
            var result = chinookContext.SaveChanges();

            // hacer la comprobación
            Assert.AreEqual(true, result > 0);
        }
    }
}
