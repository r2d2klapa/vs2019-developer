﻿using DataAccess.Repositories;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        IArtistRepository Artists { get; }
        IUserRepository Users { get; }
        IPlaylistRepository Playlists { get; }
        IRepository<Track> Tracks { get; }
        int Complete();
    }
}

