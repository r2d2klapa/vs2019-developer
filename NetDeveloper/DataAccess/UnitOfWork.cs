﻿using DataAccess.Repositories;
using Models;

namespace DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ChinookContext _context;

        public UnitOfWork(ChinookContext context)
        {
            _context = context;
            Artists = new ArtistRepository(_context);
            Albums = new Repository<Album>(_context);
            Customers = new CustomerRepository(_context);
            Employees = new Repository<Employee>(_context);
            Genres = new Repository<Genre>(_context);
            Invoices = new Repository<Invoice>(_context);
            InvoiceLines = new Repository<InvoiceLine>(_context);
            MediaTypes = new Repository<MediaType>(_context);
            Playlists = new PlaylistRepository(_context);
            Tracks = new Repository<Track>(_context);
            Users = new UserRepository(_context);
        }

        public IArtistRepository Artists { get; private set; }
        public Repository<Album> Albums { get; private set; }
        public ICustomerRepository Customers { get; private set; }
        public Repository<Employee> Employees { get; private set; }
        public Repository<Genre> Genres { get; private set; }
        public Repository<Invoice> Invoices { get; private set; }
        public Repository<InvoiceLine> InvoiceLines { get; private set; }
        public Repository<MediaType> MediaTypes { get; private set; }
        public IPlaylistRepository Playlists { get; private set; }
        public IRepository<Track> Tracks { get; private set; }

        public IUserRepository Users { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
