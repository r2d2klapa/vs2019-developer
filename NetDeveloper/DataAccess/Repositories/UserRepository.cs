﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }

        public User ValidateUser(string email, string password)
        {
            var emailParam = new SqlParameter("@email", email);
            var passParam = new SqlParameter("@password", password);
            return ChinookContext.Database.SqlQuery<ValidateUser>("dbo.SP_ValidateUser @email, @password", emailParam, passParam).Select(vu => new User { Email = vu.Email, Id = vu.Id, Roles = vu.Roles }).SingleOrDefault();
        }

        public User GetByEmail(string correo)
        {
            // devolvemos la primera ocurrencia, buscando el correo solicitado
            return ChinookContext.Users.FirstOrDefault(u => u.Email == correo);
        }
    }
}
