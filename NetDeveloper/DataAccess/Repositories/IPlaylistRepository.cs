﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IPlaylistRepository : IRepository<Playlist>
    {
        IEnumerable<Track> GetPlaylistTracks(int playlistId);
        bool AddNewTrack(Track newTrack, int playlistId);
        bool DeleteTrack(Track deleteTrack, int playlistId);
    }
}
