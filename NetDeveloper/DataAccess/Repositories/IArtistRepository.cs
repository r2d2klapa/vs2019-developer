﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IArtistRepository: IRepository<Artist>
    {
        IEnumerable<Artist> GetListArtistByStore();
        Artist GetByName(string name);
        IEnumerable<ArtistTopAlbums> GetTop10OfAlbums();
        /// <summary>
        /// Este método obtiene la lista de todos los artistas incluyendo la cantidad de álbumes de cada uno.
        /// </summary>        
        IEnumerable<ArtistTopAlbums> GetWithAlbumCount();
    }
}
