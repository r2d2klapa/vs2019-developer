﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class PlaylistRepository : Repository<Playlist>, IPlaylistRepository
    {
        public PlaylistRepository(DbContext context) : base(context)
        {
        }

        public bool AddNewTrack(Track newTrack, int playlistId)
        {
            try
            {
                // obtener la playlist
                var playlist = ChinookContext.Playlists.Find(playlistId);

                // agregar la canción al playlist                
                playlist.Tracks.Add(newTrack);

                // indicar al context que se está modificando esta entidad
                ChinookContext.Entry<Playlist>(playlist).State = EntityState.Modified;

                return true;
            }
            catch (Exception)
            {
                // en caso de error controlarlo y devolver false
                return false;
            }
        }

        public bool DeleteTrack(Track deleteTrack, int playlistId)
        {
            try
            {
                // obtener la playlist
                var playlist = ChinookContext.Playlists.Include(p => p.Tracks).FirstOrDefault(p => p.PlaylistId == playlistId);

                // agregar la canción al playlist                
                playlist.Tracks.Remove(deleteTrack);

                // indicar al context que se está modificando esta entidad
                ChinookContext.Entry<Playlist>(playlist).State = EntityState.Modified;

                return true;
            }
            catch (Exception)
            {
                // en caso de error controlarlo y devolver false
                return false;
            }
        }

        public IEnumerable<Track> GetPlaylistTracks(int playlistId)
        {
            try
            {
                // obtener la playlist incluyendo su lista de tracks
                var playlist = ChinookContext.Playlists.Include(p => p.Tracks).FirstOrDefault(p => p.PlaylistId == playlistId);

                // devolver la lista de tracks
                return playlist.Tracks.OrderBy(t => t.TrackId);
            }
            catch (Exception)
            {
                // si la consulta falla, devolver null para controlar el error
                return null;
            }
        }
    }
}
