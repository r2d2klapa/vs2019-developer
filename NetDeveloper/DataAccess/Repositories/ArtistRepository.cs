﻿using System.Collections.Generic;
using System.Linq;
using Models;
using System.Data.Entity;

namespace DataAccess.Repositories
{
    public class ArtistRepository : Repository<Artist>, IArtistRepository
    {
        public ArtistRepository(ChinookContext context) : base(context)
        {
        }
        public IEnumerable<Artist> GetListArtistByStore()
        {
            return ChinookContext.Database.SqlQuery<Artist>("GetListOfArtist");
        }
        public Artist GetByName(string name)
        {
            return ChinookContext.Artists.FirstOrDefault(artist => artist.Name == name);
        }
        public IEnumerable<ArtistTopAlbums> GetTop10OfAlbums()
        {
            return ChinookContext.Artists.Include(a => a.Albums).Select(a => new ArtistTopAlbums { Id = a.ArtistId, ArtistName = a.Name, AlbumCount = a.Albums.Count() }).OrderByDescending(t => t.AlbumCount).Take(10);
        }

        public IEnumerable<ArtistTopAlbums> GetWithAlbumCount()
        {
            return ChinookContext.Artists.Include(a => a.Albums).Select(a => new ArtistTopAlbums { Id = a.ArtistId, ArtistName = a.Name, AlbumCount = a.Albums.Count() }).OrderByDescending(t => t.AlbumCount);
        }
    }
}
