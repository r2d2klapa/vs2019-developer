﻿using DataAccess;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace InvoiceService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CustomerServiceRest" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CustomerServiceRest.svc or CustomerServiceRest.svc.cs at the Solution Explorer and start debugging.
    public class CustomerServiceRest : ICustomerServiceRest
    {
        public IEnumerable<CustomerInvoice> GetInvoice(string email, string invoiceCode)
        {
            using (var unitOfWork = new UnitOfWork(new ChinookContext()))
            {
                return unitOfWork.Customers.CustomerInvoice(email, Convert.ToInt32(invoiceCode)).ToList();
            }
        }
    }
}
