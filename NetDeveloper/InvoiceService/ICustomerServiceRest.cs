﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace InvoiceService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICustomerServiceRest" in both code and config file together.
    [ServiceContract]
    public interface ICustomerServiceRest
    {
        [OperationContract]
        [WebGet(UriTemplate = "Invoice/{email}/{invoiceCode}", ResponseFormat = WebMessageFormat.Json)]
        IEnumerable<CustomerInvoice> GetInvoice(string email, string invoiceCode);

    }
}
